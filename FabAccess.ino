/**
 * FabAccess.ino
 *
 *
 *
 * Library : *
 * - LiquidCrystal I2C by Frank de Brabander - Version 1.1.2
 * - ArduinoJson by BEnoit Blanchon - Version 5.13.2
 *
 *
 */

// Library
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <ArduinoJson.h>

// Pins definition
#define enablePin  D0                       // Serial RFID Reader > ENABLE pin
#define rxPin      D7                       // Serial RFID Reader > Serial input (connects to the RFID's SOUT pin)
#define txPin      D4                       // Serial RFID Reader > Serial output (unused)
#define Lgreen     D5
#define Lorange    D6
#define Lred       D3
#define Lblue      D4

// Application configuration
String id_equip = "2";                                      // id of this hardware equipment
String url_api = "http://fabaccess.openscop.tech/api/";     // url of back-end Django serveur

// Wifi authentification
char ssid[] = "network";                    //  wifi network SSID
char pass[] = "key";                        // wifi network password

// Constants definition
#define BUFSIZE    11                       // Serial RFID reader > Size of receive buffer (in bytes) (10-byte unique ID + null character)
#define RFID_START  0x0A                    // Serial RFID Reader > Start bytes
#define RFID_STOP   0x0D                    // Serial RFID Reader > Stop bytes

// Variables definition
int i = 0;  // counter
int status = WL_IDLE_STATUS;                // Wifi
char rfidData[BUFSIZE];                     // Serial RFID Reader > Buffer for incoming data
char offset = 0;                            // Serial RFID Reader > Offset into buffer

// Objects definition
WiFiClient client;                          // Wifi
LiquidCrystal_I2C lcd(0x3f,16,2);           // addresse 0x27 or 0x3f on I2C bus, 16 characters and 2 lines
SoftwareSerial rfidSerial =  SoftwareSerial(rxPin, txPin);  // new serial port for RFID reader


void setup() {

    // pin mode
    pinMode(Lgreen, OUTPUT);
    pinMode(Lorange, OUTPUT);
    pinMode(Lred, OUTPUT);
    pinMode(Lblue, OUTPUT);
    pinMode(enablePin, OUTPUT);
    pinMode(rxPin, INPUT);

    digitalWrite(Lblue, HIGH);

    // Serial RFID Reader
    digitalWrite(enablePin, HIGH);          // disable RFID Reader
    rfidSerial.begin(2400);                 // set the baud rate for the SoftwareSerial port

    // Serial monitor
    Serial.begin(9600);
    while (!Serial);                        // wait until ready
    Serial.println("RFID Card Reader");
    Serial.flush();                         // wait for all bytes to be transmitted to the Serial Monitor

    // LCD display
    lcd.init();
    lcd.backlight();
    lcd.setCursor(0,0);                     // first line, first column
    lcd.print("FabAccess");
    lcd.setCursor(0,1);                     // second line, first column
    lcd.print("RFID Card Reader");
    delay(2000);

    // Wifi connection
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Wifi connection...");
    WiFi.begin(ssid, pass);
    i = 0;
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        lcd.setCursor(i,1);
        lcd.print(".");
        i++;
        if (i == 17) {
            i = 0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Wifi connection...");
        }
    }
    // debug informations
    Serial.println();
    Serial.println("Wifi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    // HMI
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Wifi connected");
    lcd.setCursor(0,1);
    lcd.print(WiFi.localIP());
    delay(1000);

}


void loop() {

    // turn off the leds
    digitalWrite(Lgreen, LOW);
    digitalWrite(Lorange, LOW);
    digitalWrite(Lred, LOW);
    digitalWrite(Lblue, LOW);

    if(WiFi.status() == WL_CONNECTED) {         // The wifi network is connected

        // HMI
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Presentez votre");
        lcd.setCursor(0,1);
        lcd.print("badge, svp...");


        // Serial RFID Reader
        /*  When the RFID Reader is active and a valid RFID tag is placed with range of the reader,
            the tag's unique ID will be transmitted as a 12-byte printable ASCII string to the host
            (start byte + ID + stop byte)
            For example, for a tag with a valid ID of 0F0184F07A, the following bytes would be sent:
            0x0A, 0x30, 0x46, 0x30, 0x31, 0x38, 0x34, 0x46, 0x30, 0x37, 0x41, 0x0D
            We'll receive the ID and convert it to a null-terminated string with no start or stop byte.
        */

        offset = 0;                             // Offset into buffer
        rfidData[0] = 0;                        // Clear the buffer
        digitalWrite(enablePin, LOW);           // enable the RFID Reader

        while(1)                                // Wait for a response from the RFID Reader
        {                                       // See Arduino readBytesUntil() as an alternative solution to read data from the reader
            ESP.wdtFeed();
            if (rfidSerial.available() > 0)     // If there are any bytes available to read, then the RFID Reader has probably seen a valid tag
            {
                rfidData[offset] = rfidSerial.read();       // Get the byte and store it in our buffer
                if (rfidData[offset] == RFID_START)         // If we receive the start byte from the RFID Reader, then get ready to receive the tag's unique ID
                {
                    offset = -1;                // Clear offset (will be incremented back to 0 at the end of the loop)
                }
                else if (rfidData[offset] == RFID_STOP)     // If we receive the stop byte from the RFID Reader, then the tag's entire unique ID has been sent
                {
                    rfidData[offset] = 0;       // Null terminate the string of bytes we just received
                    break;                      // Break out of the loop
                }
                offset++;                       // Increment offset into array
                if (offset >= BUFSIZE) offset = 0;          // If the incoming data string is longer than our buffer, wrap around to avoid going out-of-bounds
            }
        }
        digitalWrite(enablePin, HIGH);          // disable RFID Reader

        // debug informations
        Serial.println(rfidData);               // The rfidData string should now contain the tag's unique ID with a null termination, so display it on the Serial Monitor
        Serial.flush();

        // HMI
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Badge");
        lcd.setCursor(6,0);
        lcd.print(rfidData);

        // Request to the server

        digitalWrite(Lblue, HIGH);
        HTTPClient http;
        http.begin(url_api + "check/");         // open connection to the server
        http.addHeader("Content-Type", "application/json");
        String value = "{\"id_equipement\":\"" + id_equip + "\",\"numero_tag\":\"" + String(rfidData) + "\"}";
        int httpCode = http.POST(value);        // send data

        const char* access;
        const char* message;
        String messageStr;

        // check code return
        if(httpCode == 200 ) {                  // code return ok
            Serial.println("POST : ok");
            String reponse = http.getString();
            Serial.println(reponse);
            // Json converter
            StaticJsonBuffer<500> jsonBuffer;
            JsonObject& repjson = jsonBuffer.parseObject(reponse);
            // check json conversion
            if(repjson.success()) {             // get data
                access = repjson["access"];
                message = repjson["message"];
                messageStr = String(message);
            } else {                            // report error
                Serial.println("parseObject() failed");
                access = "error";
                messageStr = "Impossible de traiter les donnees recues";
            }
        } else {                                // code return error
            access = "error";
            messageStr = http.errorToString(httpCode).c_str();
            Serial.print("POST : failed, error : ");
            Serial.println(messageStr);
        }
        http.end();                             // close connection to the server
        digitalWrite(Lblue, LOW);

        // report the status of the authorization > turn on the leds
        // Todo : close the relay
        if (strcmp(access,"true") == 0 ) {
            digitalWrite(Lgreen, HIGH);
        }
        if (strcmp(access,"False") == 0 ) {
            digitalWrite(Lred, HIGH);
        }
        // Todo : Process permission levels
        //  switch (access) {
        //      case '1':
        //      digitalWrite(Lgreen, HIGH);
        //        break;
        //      case '2':
        //      digitalWrite(Lred, HIGH);
        //        break;
        //  }

        // HMI
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Access : ");
        lcd.setCursor(9,0);
        lcd.print(access);
        // display long message with scrolling
        int msgLength = messageStr.length();
        i = 0;
        while (i+16 < msgLength+1) {
            lcd.setCursor(0,1);
            lcd.print(messageStr.substring(i,i+16));
            delay(150);
            i++;
        }

        delay(3000);                            // wait 3 seconds

    } else {                                    // The wifi network is NOT connected
        // HMI
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Pas de connexion");
        lcd.setCursor(0,1);
        lcd.print("au wifi");
    }

}